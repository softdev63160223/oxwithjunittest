[33mcommit 68b01253e8c456c60bb789bf7c49e70f1c18a6e3[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Mon Jul 25 01:58:27 2022 +0700

    Fix testCheckHorizontalPlayerO,XRow1,2,3Win

[33mcommit eb7c39d47c87b25621be39a96cfca511a1163dba[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Mon Jul 25 01:55:25 2022 +0700

    Fix testCheckHorizontalPlayerORow3Win

[33mcommit e07a7ceb0a903a5ffc65184f4bd508bace93ff0b[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Mon Jul 25 01:54:22 2022 +0700

    Create testCheckHorizontalPlayerORow2Win

[33mcommit e78280f9437a42b79e3fed3f5ff44d185ddd3457[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Mon Jul 25 01:53:01 2022 +0700

    Fix testCheckHorizontalPlayerORow1Win

[33mcommit e8d8e984c76a42f78661b50c05438a7867e7e024[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Mon Jul 25 01:50:26 2022 +0700

    Create testCheckHorizontalPlayerORow1Win

[33mcommit 95a8c8b9aad0f1a67d159c92354991a763d56067[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Mon Jul 25 01:48:27 2022 +0700

    Create testCheckVerticalPlayerXCol3Win

[33mcommit aa93158d630ea406288aaaa49ee3a3c86cffaaed[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Mon Jul 25 01:47:30 2022 +0700

    Create testCheckVerticalPlayerXCol2Win

[33mcommit 957c1b9827370af1a46ccb11b6e4ce1641c2a736[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Mon Jul 25 01:46:02 2022 +0700

    Create testCheckVerticalPlayerXCol1Win

[33mcommit 92f5c0aba67e70fc5a263db0eb6c697459145542[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Mon Jul 25 01:44:11 2022 +0700

    Change CheckHorizontal to Pure function

[33mcommit 33022e41d7bd9dbff7f9843e3734314f1a6383af[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Mon Jul 25 01:39:41 2022 +0700

    Create testCheckVerticalPlayerOCol3Win

[33mcommit bc4ffa43321e6b72f8b0efa72b672c875e2e72f2[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Mon Jul 25 01:38:28 2022 +0700

    Create testCheckVerticalPlayerOCol2Win

[33mcommit 06f6773f32d9a101a34096e682d4e7754d6b852f[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Mon Jul 25 01:37:16 2022 +0700

    Create testCheckVerticalPlayerOCol1Win

[33mcommit 870f9e832000187b73b2e4629ebef9829217a420[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Mon Jul 25 01:35:38 2022 +0700

    Change to Pure Function

[33mcommit b76409b590bc24a74da1d959ebe28a7cdea72f51[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Mon Jul 25 00:29:19 2022 +0700

    Create TestCheckVerticalPlayerOCol1,2,3

[33mcommit 1267935813dec096ec3b1f1f0fde1c3ee764f91e[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Mon Jul 25 00:27:55 2022 +0700

    Create TestCheckVerticalPlayerOCol1

[33mcommit 42caef6b4f8a76753fccf54613507025f93d56b0[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Sun Jul 24 23:43:19 2022 +0700

    Fix Program Because it cannot run junit test

[33mcommit 01a59ffe621bd59631394ae0883fe530e24c1634[m
Author: Sukdituch Thongmuang <focusfoyanime@gmail.com>
Date:   Wed Jul 20 08:18:40 2022 +0700

    Duplicate old program
